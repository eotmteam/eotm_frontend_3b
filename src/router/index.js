import Vue from 'vue'
import Router from 'vue-router'
import Thumbs from '@/app/thumbs/Thumbs'
import Article from '@/app/article/Article'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Thumbs',
      component: Thumbs
    },
    {
      path: '/:category/',
      name: 'Thumbs',
      component: Thumbs
    },
    {
      path: '/:category/:subcategory',
      name: 'Thumbs',
      component: Thumbs
    },
    {
      path: '/search/:word',
      name: 'Thumbs',
      component: Thumbs
    },
    {
      path: '/article/:href',
      name: 'Article',
      component: Article
    }
  ]
})
